<div class="flex p-4 {{ $loop->last ? '' : 'border-b border-b-gray-400' }} ">
    <div class="flex-shrink-0 mr-4">
        <a href="{{ route('profile', $tweet->user->path()) }}">
            <img
                class="w-12 rounded-full mr-2"
                src="{{ $tweet->user->avatar }}"
                alt=""
            >
        </a>
    </div>

    <div>
        <h5 class="font-bold mb-4">
            <a href="{{ $tweet->user->path() }}">
                {{ $tweet->user->name }}
            </a>
        </h5>
        <p class="text-sm">
            {{ $tweet->body }}
        </p>
    </div>
</div>
