@extends('components.app')

@section('content')
    <header class="mb-6 relative" style="position: relative">
        <div class="relative">
            <img
                src="/images/default-profile-banner.png"
                class="rounded-lg mb-2"
                alt="Profile Banner"
            >

            <img
                class="w-32 rounded-full absolute bottom-0 transform -translate-x-1/2 translate-y-1/2"
                style="left: 50%"
                src="{{ $user->avatar }}"
                alt=""
                width="136"
            >
        </div>

        <div class="flex justify-between items-center mb-4">
            <div>
                <h2 class="font-bold text-2xl mb-0">{{ $user->name }}</h2>
                <p class="text-sm">Joined {{ $user->created_at->diffForHumans() }}</p>
            </div>

            <div class="flex">
                @can('edit', $user)
                    <a
                        href="{{ $user->path('edit') }}"
                        class="rounded-full border border-gray-300 py-2 px-4 mr-2 text-black text-xs"
                    >
                        Edit Profile
                    </a>
                @endcan

                @include('components.follow-button')

            </div>
        </div>

        <p class="text-sm">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris accumsan nisl eu sem lobortis pharetra. Fusce iaculis feugiat turpis, ac congue nunc porttitor nec. Nullam rutrum justo libero, vitae interdum ipsum posuere sed.</p>

    </header>

    @include('_timeline', [
        'tweets' => $user->tweets
    ])
@endsection
