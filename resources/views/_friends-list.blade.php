<h3 class="font-bold text-xl mb-4">Following</h3>
<ul>
    @foreach(auth()->user()->follows as $user)
        <li>
            <div class="flex items-center text-sm">
                <a href="{{ route('profile', $user) }}" class="flex items-center text-sm">
                    <img
                        class="w-10 rounded-full mr-2 my-2"
                        src="{{ $user->avatar }}"
                        alt=""
                    >
                    {{ $user->name }}
                </a>
            </div>
        </li>
    @endforeach
</ul>


