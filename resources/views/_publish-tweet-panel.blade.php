<div class="border border-blue-400 rounded-lg py-4 px-8 mb-6">
    <form method="post" action="/tweets">
        @csrf
            <textarea
                name="body"
                class="w-full"
                placeholder="What's up doc?"
            ></textarea>

        <hr class="my-4">

        <footer class="flex justify-between">
            <img
                class="w-10 rounded-full mr-2"
                src="{{ auth()->user()->avatar }}"
                alt="Your avatar"
            >

            <button
                class="bg-blue-500 rounded-lg shadow py-2 px-2 text-white"
                type="submit"
            >
                Publish
            </button>
        </footer>
    </form>

    @error('body')
        <p class="text-red-500 text-sm mt-2">{{ $message }}</p>
    @enderror
</div>
